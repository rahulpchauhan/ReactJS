import React from 'react';


class MainSideBar extends React.Component {

  constructor(props){
    super(props);
    this.handleMenu = this.handleMenu.bind(this);
  }

  handleMenu(bindingValue){
    this.props.menuEvent(bindingValue);
  }

  render(){
    return (
      <aside className="main-sidebar">
       <section className="sidebar" style={{height: "auto"}}>
        <div className="user-panel">
         <div className="pull-left image">
          <img src={avatar} height="90" className="img-circle" alt="User Image"></img>
         </div>
         <div className="pull-left info">
          <p id="sidebar_username"></p>
          <a href="#"><i className="text-success"></i> Employee</a>
         </div>
       </div>
       <ul className="sidebar-menu">
       <li id="li_dashboard">
         <a href="#" onClick={this.handleMenu.bind(null,"dashboard")} id="dashboard">
           <i className="fa fa-dashboard"></i> <span>Dashboard</span>
         </a>
       </li>
       <li id="li_sales_performance">
          <a href="#" onClick={this.handleMenu.bind(null,"sales")} id="sales">
            <i className="fa fa-pie-chart"></i> <span>Sales Performance</span>
          </a>
        </li>
       </ul>
      </section>
      </aside>
    );
  }
}

var main_sidebar = <MainSideBar />

module.exports = main_sidebar;
