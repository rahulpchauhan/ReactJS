import './incljs.js';

import React from 'react';
import ReactDOM from 'react-dom';

import avatar from '../../dist/img/avatar5.png'

import MainSideBar from './main_sidebar.js';
import MainHeader from './main_header.js';
import MainFooter from './main_footer.js';

import Dashboard from '../module/dashboard/dashboard.js';
import SalesPerformance from '../module/sales/sales_performance.js';






class Wrapper extends React.Component {

  constructor(props){
    super(props);
    this.handleMenuEvent = this.handleMenuEvent.bind(this);
    this.state = {currentPage: 'dashboard'};
  }

  handleMenuEvent(val){
    this.setState({currentPage: val});
  }

  render(){
    var currPage = this.state.currentPage == 'dashboard' ? <Dashboard /> : <SalesPerformance />
    return (
      <div>
        <MainHeader />
        <MainSideBar menuEvent={this.handleMenuEvent} value="dashboard"/>
        {currPage}
        <MainFooter />
      </div>
    );
  }
}





ReactDOM.render(<Wrapper />, document.getElementById('wrapper'));
