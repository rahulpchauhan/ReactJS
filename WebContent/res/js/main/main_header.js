import React from 'react';

import avatar from '../../dist/img/avatar5.png'

export default class MainHeader extends React.Component {
  render(){
    return (
      <header className="main-header">
	     <a href="#" className="logo">
		     <span className="logo-mini"><b>S</b>SP</span>
		       <span className="logo-lg"><b>SlingShot</b>Power</span>
	     </a>
       <nav className="navbar navbar-static-top">
		     <a href="#" className="sidebar-toggle" data-toggle="offcanvas"
			      role="button" id="toggle_button"> <span className="sr-only">Toggle navigation</span>
		     </a>

		     <div className="navbar-custom-menu">
			      <ul className="nav navbar-nav">
              <li className="dropdown user user-menu">
                <a href="#"
					       className="dropdown-toggle" data-toggle="dropdown"> <img
						           src={avatar} className="user-image"
						                 alt="User Image"/> <span id="user_fullname" className="hidden-xs"></span>
				        </a>
					    <ul className="dropdown-menu">
						  <li className="user-header"><img src={avatar}
							className="img-circle" alt="User Image"/>

							<p id="profile_info">
								Alexander Pierce - Web Developer
							</p></li>

              <li className="user-footer">
							<div className="pull-left">
								<a href="#" className="btn btn-default btn-flat">Profile</a>
							</div>
							<div className="pull-right">
								<a href="#" className="btn btn-default btn-flat" id="sign_out_profile">Sign out</a>
							</div>
						</li>
					</ul></li>

          </ul>
		</div>
	</nav>
</header>
    );
  }
}
