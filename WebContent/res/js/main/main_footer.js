import React from 'react';

export default class MainFooter extends React.Component {
  render(){
    return (
      <footer className="main-footer">
    <div className="pull-right hidden-xs">
      <b>Version</b> 0.1
    </div>
    <strong>2017@<a href="http://grid-scape.com">Grid-Scape</a>.</strong>
  </footer>
    );
  }
}
