import React from 'react';

export default class SalesPerformance extends React.Component {
  render(){
    return (
      <div className="content-wrapper" style={{minHeight: "700px"}}>
        <section className="content-header">
			     <h1>Sales Performance</h1>
			     <ol className="breadcrumb">
				       <li className="active"><a href="#"><i className="fa fa-pie-chart"></i>Sales
						         Performance</a></li>
			     </ol>
			  </section>
      </div>
    );
  }
}
