import React from 'react';

import ContentHeader from './content_header.js';
import Content from './content.js';

export default class Dashboard extends React.Component {
  render(){
    return (
      <div className="content-wrapper" style={{minHeight: "700px"}}>
        <ContentHeader />
        <Content/>
      </div>
    );
  }
};
