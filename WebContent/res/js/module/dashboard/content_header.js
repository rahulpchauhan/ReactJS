import React from 'react';

export default class ContentHeader extends React.Component {
  render() {
    return (
      <section className="content-header">
			<h1>Dashboard</h1>
			<ol className="breadcrumb">
				<li className="active"><a href="#"> <i className="fa fa-dashboard"></i>
						Dashboard
				</a></li>
			</ol>
			</section>
    );
  }
}
