import React from 'react';

import Chatbox from './chatbox.js';

export default class CustActFeed extends React.Component {
  render(){
    return (
      <div className="col-md-6">
					<div className="box box-primary">
						<div className="box-header with-border">
							<h3 className="box-title">Customer Active Feed</h3>
							<div className="pull-right">
								<a href="#" className="text-muted"><i
									className="fa  fa-external-link-square"></i></a>
							</div>
						</div>
						<div className="box-body">
							<Chatbox />
						</div>
					</div>
				</div>
    );
  }
}
