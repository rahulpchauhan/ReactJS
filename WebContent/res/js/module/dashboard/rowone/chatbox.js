import React from 'react';

import avatar from '../../../../dist/img/avatar5.png'

export default class Chatbox extends React.Component {

  componentDidMount(){
    $('#chat-box').slimScroll({
      height : '230px'
    });
  }

  render() {
    return (
      <div className="box-body chat" id="chat-box"
								style={{overFlow: "hidden"}, {width: "auto"}}>
								<div className="item">
									<img
										src={avatar}
										alt="user image" className="online"></img>

									<p className="message">
										<a href="#" className="name"> <small
											className="text-muted pull-right"><i
												className="fa fa-clock-o"></i> 2:15</small> Mike Doe
										</a> I would like to meet you to discuss the latest news about the
										arrival of the new theme. They say it is going to be one the
										best themes on the market
									</p>
									<div className="attachment">
										<h4>Attachments:</h4>

										<p className="filename">Theme-thumbnail-image.jpg</p>

										<div className="pull-right">
											<button type="button" className="btn btn-primary btn-sm btn-flat">Open</button>
										</div>
									</div>

								</div>
								<div className="item">
									<img
										src={avatar}
										alt="user image" className="offline"></img>

									<p className="message">
										<a href="#" className="name"> <small
											className="text-muted pull-right"><i
												className="fa fa-clock-o"></i> 5:15</small> Alexander Pierce
										</a> I would like to meet you to discuss the latest news about the
										arrival of the new theme. They say it is going to be one the
										best themes on the market
									</p>
								</div>

								<div className="item">
									<img
										src={avatar}
										alt="user image" className="offline"/>

									<p className="message">
										<a href="#" className="name"> <small
											className="text-muted pull-right"><i
												className="fa fa-clock-o"></i> 5:30</small> Susan Doe
										</a> I would like to meet you to discuss the latest news about the
										arrival of the new theme. They say it is going to be one the
										best themes on the market
									</p>
								</div>

							</div>
    );
  }
}
