import React from 'react';

export default class CustActMap extends React.Component {
  render(){
    return (
      <div className="col-md-6">
					<div className="box box-primary">
						<div className="box-header with-border">
							<h3 className="box-title">Customer Activity Map</h3>
							<div className="pull-right">
								<a href="#" className="text-muted"><i
									className="fa  fa-external-link-square"></i></a>
							</div>
						</div>
						<div className="box-body">
							<iframe width="100%" height="230" frameBorder="0" style={{border: "0"}}
								src="https://www.google.com/maps/embed/v1/view?key=AIzaSyAsIfoOrp7zO1u20U40UmpDTHU9HUqDE8Y&zoom=1&center=34.0479%2C100.6197"
								allowFullScreen></iframe>
						</div>
					</div>
				</div>
    );
  }
}
