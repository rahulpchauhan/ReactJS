import React from 'react';

import CustActMap from './customer_activity_map.js';
import CustActFeed from './customer_active_feed.js';

export default class RowOne extends React.Component {
  render(){
    return (
      <div className="row">
        <CustActMap/>
        <CustActFeed />
      </div>
    );
  }
}
