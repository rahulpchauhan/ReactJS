import React from 'react';

import RowOne from './rowone/row_one.js';
import RowTwo from './rowtwo/row_two.js';

export default class Content extends React.Component {
  render(){
    return (
      <section className="content">
        <RowOne />
        <RowTwo />
      </section>
    );
  }
}
