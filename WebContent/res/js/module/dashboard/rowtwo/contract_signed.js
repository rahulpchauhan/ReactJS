import React from 'react';

export default class ContractSigned extends React.Component {

  componentDidMount(){
    $('#contract_signed').knob({
      'min': 0,
      'max':100
    });
  }

  render(){
    return (
      <div className="box box-primary">
						<div className="box-header with-border">
							<h3 className="box-title">Contracts Signed</h3>
							<div className="pull-right">
								<a href="#" className="text-muted"><i
									className="fa  fa-external-link-square"></i></a>
							</div>
						</div>
						<div className="box-body">
							<div className="col-md-5 text-center">
								<input type="text" data-fgcolor="#CC4A89" data-thickness="0.2"
									data-readonly="true" readOnly="readonly" value="55"
									className="dial" data-min="-50" data-max="50" id="contract_signed"
									data-height="50" data-width="50" />
							</div>
							<div className="col-md-7 text-center">Last Month</div>
						</div>
					</div>
    );
  }
}
