import React from 'react';

import SiteSurvey from './site_survey.js';
import ContractSigned from './contract_signed.js';

export default class DivColBox extends React.Component {
  render(){
    return (
      <div className="col-md-4">
        <SiteSurvey />
        <ContractSigned />
      </div>
    );
  }
}
