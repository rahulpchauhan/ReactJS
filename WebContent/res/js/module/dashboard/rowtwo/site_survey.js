import React from 'react';

export default class SiteSurvey extends React.Component {

  componentDidMount(){
    $('#site_survey').knob({
      'min': 0,
      'max':100
    });
  }

  render(){
    return (
      <div className="box box-primary">
						<div className="box-header with-border">
							<h3 className="box-title">Site Surveys</h3>
							<div className="pull-right">
								<a href="#" className="text-muted"><i
									className="fa  fa-external-link-square"></i></a>
							</div>
						</div>
						<div className="box-body">
							<div className="col-md-5 text-center">
								<input type="text" data-fgcolor="#BCCC4A" data-thickness="0.2"
									data-readonly="true" readOnly="readonly" value="44"
									className="dial" data-min="-50" data-max="50" id="site_survey"
									data-height="50" data-width="50" />
							</div>
							<div className="col-md-7 text-center">Last Month</div>
						</div>
					</div>
    );
  }
}
