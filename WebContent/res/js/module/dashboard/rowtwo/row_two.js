import React from 'react';

import ActiveProposal from './active_proposal.js';
import DivColBox from './divcolbox.js';
import Installations from './installations.js';

export default class RowTwo extends React.Component {
  render(){
    return (
      <div className="row">
        <ActiveProposal />
        <DivColBox />
        <Installations />
      </div>
    );
  }
}
