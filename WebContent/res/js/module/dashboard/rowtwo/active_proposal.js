import React from 'react';

export default class ActiveProposal extends React.Component {

  componentDidMount(){
    $('#active_proposal').knob({
      'min': 0,
      'max':100
    });
  }

  render(){
    return (
      <div className="col-md-4">
					<div className="box box-primary">
						<div className="box-header with-border">
							<h3 className="box-title">Active Proposals</h3>
							<div className="pull-right">
								<a href="#" className="text-muted"><i
									className="fa  fa-external-link-square"></i></a>
							</div>
						</div>
						<div className="box-body text-center">
							<input type="text" data-thickness="0.2" data-readonly="true"
								readOnly="readonly" value="65" className="dial" data-min="-50"
								data-max="50" id="active_proposal" data-height="160"
								data-width="160" />
							<div className="knob-label">This Month</div>
						</div>
					</div>
				</div>
    );
  }
}
