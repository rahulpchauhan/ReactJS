import React from 'react';

export default class Installations extends React.Component {

  componentDidMount(){
    $('#installation').knob({
      'min': 0,
      'max':100
    });
  }

  render(){
    return (
      <div className="col-md-4">
					<div className="box box-primary">
						<div className="box-header with-border">
							<h3 className="box-title">Installations</h3>
							<div className="pull-right">
								<a href="#" className="text-muted"><i
									className="fa  fa-external-link-square"></i></a>
							</div>
						</div>
						<div className="box-body text-center">
							<input type="text" data-fgcolor="#00a65a" data-thickness="0.2"
								data-readonly="true" readOnly="readonly" value="20" className="dial"
								data-min="-50" data-max="50" id="installation" data-height="160"
								data-width="160" />
							<div className="knob-label">Last Month</div>
						</div>
					</div>
				</div>
    );
  }
}
