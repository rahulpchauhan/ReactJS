<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<title>Login</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/res/js/bundle/inclcss-bundle.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
</head>
<body class="hold-transition login-page">
  
  <%-- <c:choose>
  	<c:when test="${not empty  }">
  	</c:when>
  </c:choose> --%>
  
  <div id="tempid"></div>
	<div class="login-box">
		<div class="login-logo">
			<a href="#"><b>Login</b></a>
		</div>
		<!-- /.login-logo -->
		<div class="login-box-body">
			<p class="login-box-msg">Sign in to start your session</p>

			<form name="login_form" id="login_form" method="post">
				<div class="form-group has-feedback">
					<input type="email" name="email" class="form-control"
						placeholder="Email" id="email" name="email">
					<!-- <span class="glyphicon glyphicon-envelope form-control-feedback"></span> -->
				</div>
				<div class="form-group has-feedback">
					<input type="password" name="password" class="form-control"
						placeholder="Password" name="password" id="password">
					<!-- <span class="glyphicon glyphicon-lock form-control-feedback"></span> -->
				</div>
				<div class="row">
					<div class="col-xs-8">
						<div class="checkbox icheck">
							<label class="">
								<div class="icheckbox_square-blue" aria-checked="false"
									aria-disabled="false" style="position: relative;">
									<input type="checkbox"
										style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;">
									<ins class="iCheck-helper"
										style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
								</div> Remember Me
							</label>
						</div>
					</div>
					<!-- /.col -->
					<div class="col-xs-4">
						<button type="button" class="btn btn-primary btn-block btn-flat"
							name="submit_login_form" id="submit_login_form">Sign In</button>
					</div>
					<!-- /.col -->
				</div>
			</form>

			<div class="social-auth-links text-center">
				<p>- OR -</p>
				<a
						href="https://accounts.google.com/o/oauth2/auth?scope=email&redirect_uri=http://localhost:8080/SlingShot/services/oauth&response_type=code&client_id=826183663902-kcau0s5k0b7gjeo452cpkke31jert35a.apps.googleusercontent.com&approval_prompt=force"
						class="btn btn-lg btn-social btn-google btn-sm"> <i
						class="fa fa-google-plus"></i>Sign in
					</a>
			</div>
			<!-- /.social-auth-links -->

			<a href="${pageContext.request.contextPath}/pages/forgotpassword">I
				forgot my password</a><br> <a
				href="${pageContext.request.contextPath}/pages/register"
				class="text-center">Register a new membership</a>

		</div>
		<!-- /.login-box-body -->
	</div>

	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/bundle/login-bundle.js"></script>



</body>
</html>
