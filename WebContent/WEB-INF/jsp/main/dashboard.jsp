<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- text/html; charset=ISO-8859-1;  -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<title>DashBoard</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/res/js/bundle/inclcss-bundle.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
</head>

<body class="hold-transition skin-blue sidebar-mini fixed"
	style="height: auto;">
	<div class="wrapper" id="wrapper">
		
	</div>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/bundle/app-bundle.js"></script>
</body>

</html>