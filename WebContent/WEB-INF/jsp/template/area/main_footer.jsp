<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.1
    </div>
    <strong>2017@<a href="http://grid-scape.com">Grid-Scape</a>.</strong>
  </footer>