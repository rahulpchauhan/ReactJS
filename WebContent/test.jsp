import './inclcss.js';
import './incljs.js';

import React from 'react';
import ReactDOM from 'react-dom';

import avatar from '../../dist/img/avatar5.png'

class ContentHeader extends React.Component {
  render(){
    return (
      <div>
      <section className="content-header">
			<h1>Dashboard</h1>
			<ol className="breadcrumb">
				<li className="active"><a href="#"> <i className="fa fa-dashboard"></i>
						Dashboard
				</a></li>
			</ol>
			</section>
      <Content/>
      </div>
    );
  }
}

class Content extends React.Component {
  render() {
    return (
      <section className="content">
      <div className="row">
				<div className="col-md-6">
					<div className="box box-primary">
						<div className="box-header with-border">
							<h3 className="box-title">Customer Activity Map</h3>
							<div className="pull-right">
								<a href="#" className="text-muted"><i
									className="fa  fa-external-link-square"></i></a>
							</div>
						</div>
						<div className="box-body">
							<iframe width="100%" height="230" frameBorder="0" style={{border: "0"}}
								src="https://www.google.com/maps/embed/v1/view?key=AIzaSyAsIfoOrp7zO1u20U40UmpDTHU9HUqDE8Y&zoom=1&center=34.0479%2C100.6197"
								allowFullScreen></iframe>
						</div>
					</div>
				</div>
				<div className="col-md-6">
					<div className="box box-primary">
						<div className="box-header with-border">
							<h3 className="box-title">Customer Active Feed</h3>
							<div className="pull-right">
								<a href="#" className="text-muted"><i
									className="fa  fa-external-link-square"></i></a>
							</div>
						</div>
						<div className="box-body">
							<div className="box-body chat" id="chat-box"
								style={{overFlow: "hidden"}, {width: "auto"}, {height: "250px"}}>
								<div className="item">
									<img
										src={avatar}
										alt="user image" className="online"></img>

									<p className="message">
										<a href="#" className="name"> <small
											className="text-muted pull-right"><i
												className="fa fa-clock-o"></i> 2:15</small> Mike Doe
										</a> I would like to meet you to discuss the latest news about the
										arrival of the new theme. They say it is going to be one the
										best themes on the market
									</p>
									<div className="attachment">
										<h4>Attachments:</h4>

										<p className="filename">Theme-thumbnail-image.jpg</p>

										<div className="pull-right">
											<button type="button" className="btn btn-primary btn-sm btn-flat">Open</button>
										</div>
									</div>

								</div>
								<div className="item">
									<img
										src={avatar}
										alt="user image" className="offline"></img>

									<p className="message">
										<a href="#" className="name"> <small
											className="text-muted pull-right"><i
												className="fa fa-clock-o"></i> 5:15</small> Alexander Pierce
										</a> I would like to meet you to discuss the latest news about the
										arrival of the new theme. They say it is going to be one the
										best themes on the market
									</p>
								</div>

								<div className="item">
									<img
										src={avatar}
										alt="user image" className="offline"/>

									<p className="message">
										<a href="#" className="name"> <small
											className="text-muted pull-right"><i
												className="fa fa-clock-o"></i> 5:30</small> Susan Doe
										</a> I would like to meet you to discuss the latest news about the
										arrival of the new theme. They say it is going to be one the
										best themes on the market
									</p>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
      </section>
    );
  }
}



ReactDOM.render(<ContentHeader /> , document.getElementById('content-wrapper'));

$('#chat-box').slimScroll({
		height : '230px'
	});
