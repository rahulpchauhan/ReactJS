var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: {
    app: './res/js/main/app.js',
    login: './res/js/module/login/login.js'
  },
  output: {
    path: __dirname,
    filename: './res/js/bundle/[name]-bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader!sass-loader'
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg|jpe?g|png|gif)$/,
        loader: 'url-loader?limit=10000'
      },
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['es2015','react']
        }
      }
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      moment: 'moment',
      rangy: 'rangy',
      'window.jQuery' : 'jquery',
      'window.$': 'jquery'
    }),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('development')
      }
    })
  ]
}
