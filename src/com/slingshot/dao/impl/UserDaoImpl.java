package com.slingshot.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.slingshot.dao.UserDao;
import com.slingshot.model.User;

@Component
public class UserDaoImpl implements UserDao {
	
	@Autowired
	SessionFactory sessionFactory;
		
	public short save(User user){
		try{
			Session session = sessionFactory.openSession();
			session.saveOrUpdate(user);
			session.flush();
			session.close();
			return 1;
		}catch(Exception e){
			System.out.println(e.getMessage());
			return -1;
		}		
	}
	
	public User getUser(String email, String password){
		try{
			Session session = sessionFactory.openSession();
			String hql = "from User where email = :email and password = :password";
			Query query = session.createQuery(hql);
			query.setParameter("email", email);
			query.setParameter("password", password);
			@SuppressWarnings({ "unchecked" })
			List<User> list = query.list();
			if(list.size() > 0 ){
				User user =  list.get(0);
				return user;
			} else {
				return null;
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
			return null;
		}
		
	}
	
	public User getUser(String email){
		try{
			Session session = sessionFactory.openSession();
			String hql = "from User where email = :email";
			Query query = session.createQuery(hql);
			query.setParameter("email", email);
			@SuppressWarnings("unchecked")
			List<User> list = query.list();
			if(list.size() > 0){
				return list.get(0);
			} else {
				return null;
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
			return null;
		}		
	}
	
	public List<User> getUsers(){
		try{
			Session session = sessionFactory.openSession();
			String hql = "from User";
			Query query = session.createQuery(hql);
			@SuppressWarnings("unchecked")
			List<User> list = query.list();
			return list;
		} catch(Exception e){
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	@Override
	public List<User> getUserSQL(){
		try{
			Session session = sessionFactory.openSession();
			String sql = "select * from user";
			Query query = session.createSQLQuery(sql);
			@SuppressWarnings("unchecked")
			List<User> list = query.list();
			return list;
		}catch(Exception e){
			return null;
		}
	}
}
