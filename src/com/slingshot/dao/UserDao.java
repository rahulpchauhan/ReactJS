package com.slingshot.dao;

import java.util.List;

import com.slingshot.model.User;

public interface UserDao {
    public short save(User user);

    public User getUser(String email, String password);

    public User getUser(String email);

    public List<User> getUsers();

    public List<User> getUserSQL();
}
