package com.slingshot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PageController {

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String getLogin() {
        return "main/login";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String getRegisterPage() {
        return "main/register";
    }

    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public String getDashboardPage() {
        return "main/dashboard";
    }

    @RequestMapping(value = "/googlemap", method = RequestMethod.GET)
    public String getGoogleMap() {
        return "main/googlemap";
    }

    @RequestMapping(value = "/forgotpassword", method = RequestMethod.GET)
    public String getForgotPassword() {
        return "main/forgotpassword";
    }

    @RequestMapping(value = "/sales_performance", method = RequestMethod.GET)
    public String getSales_Performance() {
        return "sales/sales_performance";
    }

    @RequestMapping(value = "/monthly", method = RequestMethod.GET)
    public String getMonthlyResult() {
        return "results/monthly";
    }

    @RequestMapping(value = "/environmental", method = RequestMethod.GET)
    public String getEnvironmentalResult() {
        return "results/environmental";
    }

    @RequestMapping(value = "/marketing", method = RequestMethod.GET)
    public String getMarketingResult() {
        return "results/marketing";
    }

    @RequestMapping(value = "/googlelogin", method = RequestMethod.GET)
    public String getGoogleSignIn() {
        return "main/googlelogin";
    }
}
