package com.slingshot.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FileController {
	
	@RequestMapping(value="/handleFile", method=RequestMethod.POST)
	@ResponseBody
	public String handleFile(HttpServletRequest request) throws IOException{
		System.out.println("handle file");
		System.out.println("Content Length: "+request.getContentLength()+
				"\nAuth Type: "+request.getAuthType()+
				"\nContent Type: "+request.getContentType()+
				"\nRequest URI: "+request.getRequestURI());
		System.out.println();
		System.out.println();
		System.out.println();
		BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
		String line="",out=""; int n = 1;
		while((line = reader.readLine()) != null){
			out += line;
			System.out.println("Line: "+n+"-"+line);
			n++;
		}
		System.out.println("Output: "+out);
		return "Done";
	}
	
}
