package com.slingshot.controller;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.slingshot.client.HttpServiceHandler;
import com.slingshot.constant.Constant;
import com.slingshot.generator.ResponseGenerator;
import com.slingshot.model.User;
import com.slingshot.service.UserService;

@Controller
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    ResponseGenerator responseGenerator;

    @Autowired
    HttpServiceHandler httpServiceHandler;

    @RequestMapping(value = "/doregister", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> doRegister(@RequestBody User rbuser) throws IOException {
        if (rbuser != null && rbuser.getEmail() != null && rbuser.getPassword() != null) {
            if (userService.getUser(rbuser.getEmail()) == null && userService.registerUser(rbuser) != -1) {
                return responseGenerator.generate(Constant.RESPONSE_STATUS_COMPLETE,
                        Constant.RESPONSE_DATA_AVAILABILITY_NO, null, null);
            } else {
                return responseGenerator.generate(Constant.RESPONSE_STATUS_COMPLETE,
                        Constant.RESPONSE_DATA_AVAILABILITY_YES, null, null);
            }
        } else {
            return responseGenerator.generate(Constant.RESPONSE_STATUS_INCOMPLETE,
                    Constant.RESPONSE_DATA_AVAILABILITY_NO, null,
                    Constant.RESPONSE_STATUS_INCOMPETE_REASON_DES.INVALID_REQUEST);
        }
    }

    @RequestMapping(value = "/dologin", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> doLogin(@RequestBody User rbuser, HttpServletRequest request) throws IOException {

        HttpSession session = request.getSession(true);


        boolean rbuser_null = false;
        boolean user_null = false;

        User user = null;

        if (rbuser != null && rbuser.getEmail() != null && rbuser.getPassword() != null) {
            user = userService.loginUser(rbuser);
            user_null = (user == null) ? true : false;
        } else {
            rbuser_null = true;
        }

        if (rbuser_null || user_null) {
            return responseGenerator.generate(Constant.RESPONSE_STATUS_COMPLETE, Constant.RESPONSE_DATA_AVAILABILITY_NO,
                    null, null);
        } else {
            return responseGenerator.generate(Constant.RESPONSE_STATUS_COMPLETE,
                    Constant.RESPONSE_DATA_AVAILABILITY_YES, user, null);
        }
    }

    @RequestMapping(value = "/recoverpass", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> doRecoverPass(@RequestBody User rbuser) {
        if (rbuser != null && rbuser.getEmail() != null && userService.getUser(rbuser.getEmail()) != null
                && userService.recoverPass(userService.getUser(rbuser.getEmail()))) {
            User user = userService.getUser(rbuser.getEmail());
            return responseGenerator.generate(Constant.RESPONSE_STATUS_COMPLETE,
                    Constant.RESPONSE_DATA_AVAILABILITY_YES, user, null);
        } else {
            return responseGenerator.generate(Constant.RESPONSE_STATUS_INCOMPLETE,
                    Constant.RESPONSE_DATA_AVAILABILITY_NO, null,
                    Constant.RESPONSE_STATUS_INCOMPETE_REASON_DES.INTERNAL_ERROR_OCCURED);
        }
    }

    @RequestMapping(value = "/getusers", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getUsers() {
        return responseGenerator.generate(Constant.RESPONSE_STATUS_COMPLETE, Constant.RESPONSE_DATA_AVAILABILITY_YES,
                userService.getUsers(), null);
    }

    @RequestMapping(value = "/session", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> doSession(HttpServletRequest request, HttpServletResponse response, @RequestBody JsonNode datab) {
        HttpSession httpSession = request.getSession();
        String sessionId = httpSession.getId();
        ObjectNode node = new ObjectMapper().createObjectNode();
        node.put("prevSessionId", datab.get("sessionid").asText());
        node.put("currentSessionId", sessionId);
        node.put("equals", datab.get("sessionid").asText().equals(sessionId));
        return responseGenerator.generate(Constant.RESPONSE_STATUS_COMPLETE, Constant.RESPONSE_DATA_AVAILABILITY_NO, node, null);
    }

    @RequestMapping(value = "/oauth2", method = RequestMethod.GET)
    public String doOAuth(HttpServletRequest request, HttpServletResponse response) {
        String code = request.getParameter("code");
        ObjectNode node1 = null;
        try {
            String parameters = "code=" + code + "&client_id=" + Constant.CLIENT_ID + "&client_secret=" + Constant.CLIENT_SECRET + "&redirect_uri=" + Constant.REDIRECT_URL_OAUTH + "&grant_type=authorization_code";
            URL url = new URL("https://accounts.google.com/o/oauth2/token");
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
            writer.write(parameters);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String makeString = "";
            ObjectMapper mapper = new ObjectMapper();
            String line;
            while ((line = reader.readLine()) != null) {
                makeString = makeString + line;
            }
            System.out.println(makeString);
            JsonNode jsonNode = mapper.readTree(makeString);
            String token = jsonNode.get("access_token").asText();

            URL urlAccessToken = new URL(Constant.ACCESSTOKEN_URL + "=" + token);

            conn = urlAccessToken.openConnection();
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            makeString = "";
            line = "";
            while ((line = reader.readLine()) != null) {
                makeString += line;
            }
            System.out.println(makeString);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return "main/dashboard";

    }

    @RequestMapping(value = "/oauth", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody()
    public JsonNode doAuth(HttpServletRequest request) {
        return userService.performOAuth(request.getParameter("code"));
    }

    @RequestMapping(value = "/getusersql", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> getUserSQL() {
        return responseGenerator.generate(Constant.RESPONSE_STATUS_COMPLETE, true, userService.getUserSQL(), null);
    }

    @RequestMapping(value = "/userstest", method = RequestMethod.GET)
    @ResponseBody
    public String usertest() {
        httpServiceHandler.sendRequest();
        System.out.println("called");
        return "done";
    }

    @ExceptionHandler({org.springframework.http.converter.HttpMessageNotReadableException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Map<String, Object> resolveException() {
        return responseGenerator.generate(Constant.RESPONSE_STATUS_INCOMPLETE, Constant.RESPONSE_DATA_AVAILABILITY_NO,
                null, Constant.RESPONSE_STATUS_INCOMPETE_REASON_DES.PROCESS_REQUIRED_VALID_BODY);
    }


}
