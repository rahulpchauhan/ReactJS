package com.slingshot.drive;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.gspsu.app.GSPSUApp;


@WebListener
public class DriveContent implements ServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {

    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        GSPSUApp.main(new String[]{});
        System.out.println("hello");
    }

}
