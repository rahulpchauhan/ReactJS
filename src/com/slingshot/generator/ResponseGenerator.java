package com.slingshot.generator;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.slingshot.constant.Constant;

@Component
@Scope("singleton")
public class ResponseGenerator {

    private Map<String, Object> response = null;

    public Map<String, Object> generate(String res_status, boolean res_dataavailability, Object res_data, Constant.RESPONSE_STATUS_INCOMPETE_REASON_DES res_ic_reason) {
        response = new HashMap<String, Object>();
        response.put(Constant.RESPONSE_STATUS, res_status);
        response.put(Constant.RESPONSE_DATA_AVAILABILITY, res_dataavailability);
        response.put(Constant.RESPONSE_DATA, res_data);
        response.put(Constant.RESPONSE_STATUS_INCOMPLETE_REASON, res_ic_reason);
        return response;
    }

}
