package com.slingshot.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public class DefaultEntry implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 528558680026561438L;

    public DefaultEntry() {

    }

    public DefaultEntry(long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue
    @Column(name = "id")
    private long id;

    @Column(name = "created_at", insertable = false, updatable = false, columnDefinition = "timestamp default current_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created_at;

    @Column(name = "updated_at", insertable = true, updatable = true, columnDefinition = "timestamp default current_timestamp")
    private Date updated_at;

    @Column(name = "updated_by")
    private int updated_by;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public int getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(int updated_by) {
        this.updated_by = updated_by;
    }


}
