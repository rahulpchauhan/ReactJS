package com.slingshot.client;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class HttpServiceHandler {

    public void sendRequest() {
        HttpURLConnection conn = null;

        try {
            URL url = new URL("http://DESKTOP-8DQ8HD8:8086/getusers");
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            System.out.println(conn.getResponseCode());
            InputStream stream = conn.getInputStream();
            Reader reader = new InputStreamReader(stream);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String data = "", response = "";
            while ((data = bufferedReader.readLine()) != null) response += data;

            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode jsonNode = objectMapper.readTree(response);
            System.out.println(jsonNode.get("name").asText());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
