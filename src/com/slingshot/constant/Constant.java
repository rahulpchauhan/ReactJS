package com.slingshot.constant;

public interface Constant {

    public final String PROJECT_NAME = "SlingShot";
    public final String DBNAME = "slingslot";

    public final String RESPONSE_STATUS = "status";
    public final String RESPONSE_DATA = "data";

    public final String RESPONSE_STATUS_ERROR = "error";
    public final String RESPONSE_STATUS_COMPLETE = "complete";
    public static final String RESPONSE_STATUS_INCOMPLETE = "incomplete";
    public static final String RESPONSE_STATUS_INCOMPLETE_REASON = "incomplete_reason";

    public enum RESPONSE_STATUS_INCOMPETE_REASON_DES {
        INVALID_REQUEST,
        PROCESS_REQUIRED_VALID_BODY,
        INTERNAL_ERROR_OCCURED
    }

    public static final String RESPONSE_DATA_AVAILABILITY = "data_availability";

    public static final boolean RESPONSE_DATA_AVAILABILITY_YES = true;
    public static final boolean RESPONSE_DATA_AVAILABILITY_NO = false;

    public static final String SMTP_GMAIL = "smtp.gmail.com";
    public static final String SMTP_AUTH = "mail.smtp.auth";
    public static final String SMTP_STARTTLS = "mail.smtp.starttls.enable";
    public static final String SMTP_HOST = "mail.smtp.host";
    public static final String SMTP_PORT = "mail.smtp.port";

    public static final String DEFAULT_PASS = "GPNPMLOGIN";

    public static final String MAIL_SUBJECT_PASS_RECOVERY = "Password Recovery";

    public static final String PASS_RECOVERY_MESSAGE = "Hello<br/>Your password for demo.grid-scape.com is <b>" + DEFAULT_PASS + "</b>";

    public static final String CLIENT_ID = "826183663902-kcau0s5k0b7gjeo452cpkke31jert35a.apps.googleusercontent.com";

    public static final String CLIENT_SECRET = "ejCJquaoHmEfbX6jpb-QhROw";

    public static final String REDIRECT_URL_OAUTH = "http://localhost:8080/SlingShot/services/oauth";

    public static final String TOKEN_URL = "https://accounts.google.com/o/oauth2/token";

    public static final String ACCESSTOKEN_URL = "https://www.googleapis.com/oauth2/v1/userinfo?access_token";

    public static final String TOKEN_PARA = "client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET + "&redirect_uri=" + REDIRECT_URL_OAUTH + "&grant_type=authorization_code";


}
