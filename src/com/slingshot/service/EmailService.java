package com.slingshot.service;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.context.annotation.Scope;

import com.slingshot.constant.Constant;

@Scope("singleton")
public class EmailService {

    private String emailid;
    private String username;
    private String password;

    Session session = Session.getInstance(getProperties(),
            new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

    public String getEmailid() {
        return emailid;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private Properties getProperties() {
        Properties properties = new Properties();
        properties.put(Constant.SMTP_AUTH, "true");
        properties.put(Constant.SMTP_STARTTLS, "true");
        properties.put(Constant.SMTP_HOST, "smtp.gmail.com");
        properties.put(Constant.SMTP_PORT, "587");
        return properties;
    }

    public boolean send(String subject, String emailid_to, String email_body) {
        Message message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress(emailid));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailid_to));
            message.setSubject(subject);
            message.setContent(email_body, "text/html");
            Transport.send(message);
            return true;
        } catch (MessagingException me) {
            System.out.println(me.getMessage());
            return false;
        }
    }


}
