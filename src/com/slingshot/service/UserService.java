package com.slingshot.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.slingshot.constant.Constant;
import com.slingshot.dao.UserDao;
import com.slingshot.manager.CustomSecurityManager;
import com.slingshot.model.User;

@Service
public class UserService {

    @Autowired
    UserDao userDao;

    @Autowired
    CustomSecurityManager securityManager;

    @Autowired
    EmailService emailService;

    public short save(User user) {
        return userDao.save(user);
    }

    public User getUser(String email, String password) {
        return userDao.getUser(email, password);
    }

    public User getUser(String email) {
        return userDao.getUser(email);
    }

    public User getUserByEmailPassword(User rbuser) {
        return this.getUser(rbuser.getEmail(), securityManager.encryptPassword(rbuser.getPassword()));
    }

    public List<User> getUsers() {
        return userDao.getUsers();
    }

    public void setToken(User user) {
        user.setToken(securityManager.getToken());
    }

    public short registerUser(User rbuser) {
        User user = new User();
        user.setFullname(rbuser.getFullname());
        user.setPassword(securityManager.encryptPassword(rbuser.getPassword()));
        user.setEmail(rbuser.getEmail());
        return this.save(user);
    }

    public User loginUser(User rbuser) {
        User user = this.getUser(rbuser.getEmail(), securityManager.encryptPassword(rbuser.getPassword()));
        if (user != null) {
            this.setToken(user);
            return user;
        } else {
            return null;
        }
    }

    public boolean recoverPass(User user) {
        try {
            user.setPassword(securityManager.encryptPassword(Constant.DEFAULT_PASS));
            this.save(user);
            emailService.send(Constant.MAIL_SUBJECT_PASS_RECOVERY, user.getEmail(), Constant.PASS_RECOVERY_MESSAGE);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public EmailService getEmailService() {
        return emailService;
    }

    public List<User> getUserSQL() {
        return userDao.getUserSQL();
    }

    public JsonNode performOAuth(String code) {
        try {
            String tokenPara = "code=" + code + "&" + Constant.TOKEN_PARA;

            URL tokenUrl = new URL(Constant.TOKEN_URL);
            URLConnection tokenConnection = tokenUrl.openConnection();
            tokenConnection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(tokenConnection.getOutputStream());
            writer.write(tokenPara);
            writer.flush();

            JsonNode node = null;
            String access_token;
            if ((node = getDataFromConnection(tokenConnection)) != null) {
                access_token = node.get("access_token").asText();
            } else {
                return null;
            }

            URL accessTokenUrl = new URL(Constant.ACCESSTOKEN_URL + "=" + access_token);
            URLConnection accessTokenConnection = accessTokenUrl.openConnection();
            return getDataFromConnection(accessTokenConnection);

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public JsonNode getDataFromConnection(URLConnection connection) {
        String data = "";
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null) {
                data += line;
            }
            JsonNode node = new ObjectMapper().readTree(data);
            return node;
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return null;
        }

    }
}
